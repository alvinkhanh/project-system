-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 05, 2021 at 08:32 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qhse`
--

-- --------------------------------------------------------

--
-- Table structure for table `aplikasi`
--

CREATE TABLE `aplikasi` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_owner` varchar(100) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `tlp` varchar(50) DEFAULT NULL,
  `title` varchar(20) DEFAULT NULL,
  `nama_aplikasi` varchar(100) DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `copy_right` varchar(50) DEFAULT NULL,
  `versi` varchar(20) DEFAULT NULL,
  `tahun` year(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `aplikasi`
--

INSERT INTO `aplikasi` (`id`, `nama_owner`, `alamat`, `tlp`, `title`, `nama_aplikasi`, `logo`, `copy_right`, `versi`, `tahun`) VALUES
(1, 'PE', 'JL. Rawabali', '0812-9936-9059', 'QHSE', 'YKK ZIPCO', 'admin1.png', 'Copy Right &copy;', '1.0.0.0', 2022);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akses_menu`
--

CREATE TABLE `tbl_akses_menu` (
  `id` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL,
  `view_level` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akses_menu`
--

INSERT INTO `tbl_akses_menu` (`id`, `id_level`, `id_menu`, `view_level`) VALUES
(1, 1, 1, 'Y'),
(2, 1, 2, 'Y'),
(43, 4, 1, 'Y'),
(44, 4, 2, 'N'),
(62, 5, 1, 'Y'),
(63, 5, 2, 'N'),
(67, 1, 53, 'Y'),
(68, 4, 53, 'N'),
(69, 5, 53, 'Y'),
(73, 6, 1, 'Y'),
(74, 6, 2, 'N'),
(75, 6, 53, 'Y'),
(80, 1, 56, 'Y'),
(81, 4, 56, 'N'),
(82, 5, 56, 'N'),
(83, 6, 56, 'Y'),
(84, 1, 57, 'Y'),
(85, 4, 57, 'N'),
(86, 5, 57, 'N'),
(87, 6, 57, 'Y'),
(88, 8, 1, 'N'),
(89, 8, 2, 'N'),
(90, 8, 53, 'N'),
(91, 8, 56, 'N'),
(92, 8, 57, 'N'),
(98, 9, 1, 'N'),
(99, 9, 2, 'N'),
(100, 9, 53, 'N'),
(101, 9, 56, 'N'),
(102, 9, 57, 'N'),
(103, 10, 1, 'N'),
(104, 10, 2, 'N'),
(105, 10, 53, 'N'),
(106, 10, 56, 'N'),
(107, 10, 57, 'N'),
(108, 11, 1, 'N'),
(109, 11, 2, 'N'),
(110, 11, 53, 'N'),
(111, 11, 56, 'N'),
(112, 11, 57, 'N'),
(113, 2, 1, 'N'),
(114, 2, 2, 'N'),
(115, 2, 53, 'N'),
(116, 2, 56, 'N'),
(117, 2, 57, 'N'),
(118, 3, 1, 'N'),
(119, 3, 2, 'N'),
(120, 3, 53, 'N'),
(121, 3, 56, 'N'),
(122, 3, 57, 'N'),
(123, 4, 1, 'N'),
(124, 4, 2, 'N'),
(125, 4, 53, 'N'),
(126, 4, 56, 'N'),
(127, 4, 57, 'N'),
(128, 5, 1, 'N'),
(129, 5, 2, 'N'),
(130, 5, 53, 'N'),
(131, 5, 56, 'N'),
(132, 5, 57, 'N'),
(133, 6, 1, 'N'),
(134, 6, 2, 'N'),
(135, 6, 53, 'N'),
(136, 6, 56, 'N'),
(137, 6, 57, 'N'),
(138, 7, 1, 'N'),
(139, 7, 2, 'N'),
(140, 7, 53, 'N'),
(141, 7, 56, 'N'),
(142, 7, 57, 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_akses_submenu`
--

CREATE TABLE `tbl_akses_submenu` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_submenu` int(11) NOT NULL,
  `view_level` enum('Y','N') DEFAULT 'N',
  `add_level` enum('Y','N') DEFAULT 'N',
  `edit_level` enum('Y','N') DEFAULT 'N',
  `delete_level` enum('Y','N') DEFAULT 'N',
  `print_level` enum('Y','N') DEFAULT 'N',
  `upload_level` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_akses_submenu`
--

INSERT INTO `tbl_akses_submenu` (`id`, `id_level`, `id_submenu`, `view_level`, `add_level`, `edit_level`, `delete_level`, `print_level`, `upload_level`) VALUES
(2, 1, 2, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(4, 1, 1, 'Y', 'N', 'N', 'N', 'N', 'N'),
(6, 1, 7, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(7, 1, 8, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(9, 1, 10, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(13, 1, 14, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(26, 1, 15, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(30, 1, 17, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(32, 1, 18, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(34, 1, 19, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(36, 1, 20, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(59, 4, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(60, 4, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(61, 4, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(62, 4, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(63, 4, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(64, 4, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(65, 4, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(66, 4, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(67, 4, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(68, 4, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(72, 5, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(73, 5, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(74, 5, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(75, 5, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(76, 5, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(77, 5, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(78, 5, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(79, 5, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(80, 5, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(81, 5, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(88, 6, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(89, 6, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(90, 6, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(91, 6, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(92, 6, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(93, 6, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(94, 6, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(95, 6, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(96, 6, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(97, 6, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(100, 1, 25, 'Y', 'Y', 'Y', 'Y', 'Y', 'Y'),
(101, 4, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(102, 5, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(103, 6, 25, 'Y', 'N', 'N', 'N', 'N', 'N'),
(104, 1, 26, 'Y', 'N', 'N', 'N', 'N', 'N'),
(105, 4, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(106, 5, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(107, 6, 26, 'Y', 'N', 'N', 'N', 'N', 'N'),
(116, 8, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(117, 8, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(118, 8, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(119, 8, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(120, 8, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(121, 8, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(122, 8, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(123, 8, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(124, 8, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(125, 8, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(126, 8, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(127, 8, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(130, 8, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(131, 6, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(132, 5, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(133, 4, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(134, 1, 29, 'Y', 'N', 'N', 'N', 'N', 'N'),
(135, 8, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(136, 6, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(137, 5, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(138, 4, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(139, 1, 30, 'Y', 'N', 'N', 'N', 'N', 'N'),
(140, 9, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(141, 9, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(142, 9, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(143, 9, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(144, 9, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(145, 9, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(146, 9, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(147, 9, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(148, 9, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(149, 9, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(150, 9, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(151, 9, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(152, 9, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(153, 9, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(154, 10, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(155, 10, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(156, 10, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(157, 10, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(158, 10, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(159, 10, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(160, 10, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(161, 10, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(162, 10, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(163, 10, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(164, 10, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(165, 10, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(166, 10, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(167, 10, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(168, 11, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(169, 11, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(170, 11, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(171, 11, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(172, 11, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(173, 11, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(174, 11, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(175, 11, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(176, 11, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(177, 11, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(178, 11, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(179, 11, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(180, 11, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(181, 11, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(182, 2, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(183, 2, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(184, 2, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(185, 2, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(186, 2, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(187, 2, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(188, 2, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(189, 2, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(190, 2, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(191, 2, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(192, 2, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(193, 2, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(194, 2, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(195, 2, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(196, 3, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(197, 3, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(198, 3, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(199, 3, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(200, 3, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(201, 3, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(202, 3, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(203, 3, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(204, 3, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(205, 3, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(206, 3, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(207, 3, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(208, 3, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(209, 3, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(210, 4, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(211, 4, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(212, 4, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(213, 4, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(214, 4, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(215, 4, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(216, 4, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(217, 4, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(218, 4, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(219, 4, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(220, 4, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(221, 4, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(222, 4, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(223, 4, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(224, 5, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(225, 5, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(226, 5, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(227, 5, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(228, 5, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(229, 5, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(230, 5, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(231, 5, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(232, 5, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(233, 5, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(234, 5, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(235, 5, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(236, 5, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(237, 5, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(238, 6, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(239, 6, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(240, 6, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(241, 6, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(242, 6, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(243, 6, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(244, 6, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(245, 6, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(246, 6, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(247, 6, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(248, 6, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(249, 6, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(250, 6, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(251, 6, 30, 'N', 'N', 'N', 'N', 'N', 'N'),
(252, 7, 1, 'N', 'N', 'N', 'N', 'N', 'N'),
(253, 7, 2, 'N', 'N', 'N', 'N', 'N', 'N'),
(254, 7, 7, 'N', 'N', 'N', 'N', 'N', 'N'),
(255, 7, 8, 'N', 'N', 'N', 'N', 'N', 'N'),
(256, 7, 10, 'N', 'N', 'N', 'N', 'N', 'N'),
(257, 7, 15, 'N', 'N', 'N', 'N', 'N', 'N'),
(258, 7, 17, 'N', 'N', 'N', 'N', 'N', 'N'),
(259, 7, 18, 'N', 'N', 'N', 'N', 'N', 'N'),
(260, 7, 19, 'N', 'N', 'N', 'N', 'N', 'N'),
(261, 7, 20, 'N', 'N', 'N', 'N', 'N', 'N'),
(262, 7, 25, 'N', 'N', 'N', 'N', 'N', 'N'),
(263, 7, 26, 'N', 'N', 'N', 'N', 'N', 'N'),
(264, 7, 29, 'N', 'N', 'N', 'N', 'N', 'N'),
(265, 7, 30, 'N', 'N', 'N', 'N', 'N', 'N');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_bidang`
--

CREATE TABLE `tbl_bidang` (
  `id_bidang` int(11) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `nama_bidang` varchar(50) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_bidang`
--

INSERT INTO `tbl_bidang` (`id_bidang`, `id_departement`, `nama_bidang`, `status`, `created_at`) VALUES
(1, 1, 'HSE', '0', '2021-12-05 14:24:06'),
(2, 1, 'QUALITY', '0', '2021-12-05 14:26:17'),
(3, 2, 'WEAVING', '0', '2021-12-05 14:26:51'),
(4, 2, 'FORMING', '0', '2021-12-05 14:26:57'),
(5, 2, 'SEWING', '0', '2021-12-05 14:27:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_departement`
--

CREATE TABLE `tbl_departement` (
  `id_departement` int(11) NOT NULL,
  `nama_departement` varchar(50) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_departement`
--

INSERT INTO `tbl_departement` (`id_departement`, `nama_departement`, `id_bidang`, `status`, `created_at`) VALUES
(1, 'QHSE', 0, '0', '2021-12-05 14:24:59'),
(2, 'NAT - CHAIN', 0, '0', '2021-12-05 14:25:16'),
(3, 'SET DYE CHAIN', 0, '0', '2021-12-05 14:25:28'),
(4, 'SHIPPING', 0, '0', '2021-12-05 14:25:36'),
(5, 'GENERAL', 0, '0', '2021-12-05 14:25:40'),
(6, 'PURCHASING', 0, '0', '2021-12-05 14:25:48'),
(7, 'UTILITY', 0, '0', '2021-12-05 14:25:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_dokumen`
--

CREATE TABLE `tbl_detail_dokumen` (
  `id_detail_dokumen` int(11) NOT NULL,
  `id_dokumen` int(11) NOT NULL,
  `id_approval` int(11) NOT NULL,
  `kode_dokumen` varchar(50) NOT NULL,
  `nama_dokumen` varchar(50) NOT NULL,
  `berkas` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `hirarki` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_detail_dokumen`
--

INSERT INTO `tbl_detail_dokumen` (`id_detail_dokumen`, `id_dokumen`, `id_approval`, `kode_dokumen`, `nama_dokumen`, `berkas`, `status`, `hirarki`, `created_at`) VALUES
(1, 0, 0, 'whhyyy', 'whyy', 'whyy.xlsx', 0, 0, '2021-12-05 20:29:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dokumen`
--

CREATE TABLE `tbl_dokumen` (
  `id_dokumen` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `kode_dokumen` varchar(15) NOT NULL,
  `id_departement` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama_dokumen` varchar(50) NOT NULL,
  `berkas` varchar(255) NOT NULL,
  `is_active` enum('Y','N') NOT NULL,
  `last_update` datetime NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_dokumen`
--

INSERT INTO `tbl_dokumen` (`id_dokumen`, `id_level`, `id_jenis`, `kode_dokumen`, `id_departement`, `id_user`, `nama_dokumen`, `berkas`, `is_active`, `last_update`, `created_at`) VALUES
(9, 0, 1, 'whhyyy', 1, 1, 'whyy', 'whyy.xlsx', 'Y', '0000-00-00 00:00:00', '2021-12-05 20:29:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_hirarki`
--

CREATE TABLE `tbl_hirarki` (
  `id_hirarki` int(11) NOT NULL,
  `nama_hirarki` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_hirarki`
--

INSERT INTO `tbl_hirarki` (`id_hirarki`, `nama_hirarki`, `created_at`) VALUES
(1, 'level 1', '2021-11-30 13:28:28'),
(2, 'level 2', '2021-11-30 13:28:28'),
(3, 'level 3', '2021-11-30 13:28:28'),
(4, 'level 4', '2021-11-30 13:28:28'),
(5, 'all', '2021-11-30 13:28:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_dokumen`
--

CREATE TABLE `tbl_jenis_dokumen` (
  `id_jenis` int(11) NOT NULL,
  `nama_jenis` text NOT NULL,
  `status` char(4) NOT NULL DEFAULT '1',
  `id_bidang` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_leveld` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_jenis_dokumen`
--

INSERT INTO `tbl_jenis_dokumen` (`id_jenis`, `nama_jenis`, `status`, `id_bidang`, `id_user`, `id_leveld`, `created_at`) VALUES
(1, 'ISO Integrated', '1', 1, 12, 3, '2021-12-05 14:43:18'),
(2, 'Environment', '1', 1, 12, 3, '2021-12-05 14:43:36'),
(3, 'Safety', '1', 1, 13, 3, '2021-12-05 14:44:00'),
(4, 'Quality', '1', 2, 14, 3, '2021-12-05 14:44:22'),
(5, 'Higienis', '1', 1, 12, 3, '2021-12-05 14:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_level_dokumen`
--

CREATE TABLE `tbl_level_dokumen` (
  `id_leveld` int(11) NOT NULL,
  `nama_leveld` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_level_dokumen`
--

INSERT INTO `tbl_level_dokumen` (`id_leveld`, `nama_leveld`, `created_at`) VALUES
(1, 'level 1', '2021-12-04 21:38:03'),
(2, 'level 2', '2021-12-04 21:38:18'),
(3, 'level 3', '2021-12-04 21:38:24');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_menu`
--

CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL,
  `nama_menu` varchar(50) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `urutan` bigint(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `parent` enum('Y') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_menu`
--

INSERT INTO `tbl_menu` (`id_menu`, `nama_menu`, `link`, `icon`, `urutan`, `is_active`, `parent`) VALUES
(1, 'Dashboard', 'dashboard', 'fas fa-tachometer-alt', 1, 'Y', 'Y'),
(2, 'System', '#', 'fas fa-cogs', 2, 'Y', 'Y'),
(53, 'Dokumen', 'Created', 'fas fa-tasks', 4, 'Y', 'Y'),
(56, 'konfirmasi dokumen', 'aprove', 'fas fa-thumbs-up', 5, 'Y', 'Y'),
(57, 'Management Dokumen', '#', 'far fa-plus-square', 3, 'Y', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_progres`
--

CREATE TABLE `tbl_progres` (
  `id_progres` int(11) NOT NULL,
  `nama_progres` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_progres`
--

INSERT INTO `tbl_progres` (`id_progres`, `nama_progres`) VALUES
(1, 'Pengajuan Dokumen'),
(2, 'verifikasi Manager departement'),
(3, 'verifikasi admin QHSE'),
(4, 'verifikasi Manager QHSE'),
(5, 'Revisi Dokumen'),
(6, 'Pengajuan Ditolak'),
(7, 'Edit Dokumen'),
(8, 'Hapus Data'),
(9, 'Aktivasi');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_progres_pengajuan`
--

CREATE TABLE `tbl_progres_pengajuan` (
  `id_progres_pengajuan` int(11) NOT NULL,
  `id_dokumen` int(11) NOT NULL,
  `ordinal` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `catatan` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_submenu`
--

CREATE TABLE `tbl_submenu` (
  `id_submenu` int(11) UNSIGNED NOT NULL,
  `nama_submenu` varchar(50) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `id_menu` int(11) DEFAULT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_submenu`
--

INSERT INTO `tbl_submenu` (`id_submenu`, `nama_submenu`, `link`, `icon`, `id_menu`, `is_active`) VALUES
(1, 'Menu', 'menu', 'far fa-circle', 2, 'Y'),
(2, 'SubMenu', 'submenu', 'far fa-circle', 2, 'Y'),
(7, 'Aplikasi', 'aplikasi', 'far fa-circle', 2, 'Y'),
(8, 'User', 'user', 'far fa-circle', 2, 'Y'),
(10, 'User Level', 'userlevel', 'far fa-circle', 2, 'Y'),
(15, 'Barang', 'barang', 'far fa-circle', 32, 'Y'),
(17, 'Kategori', 'kategori', 'far fa-circle', 32, 'Y'),
(18, 'Satuan', 'satuan', 'far fa-circle', 32, 'Y'),
(19, 'Pembelian', 'pembelian', 'far fa-circle', 41, 'Y'),
(20, 'Penjualan', 'penjualan', 'far fa-circle', 41, 'Y'),
(25, 'Bidang', 'bidang', 'fas fa-briefcase', 57, 'Y'),
(26, 'Tambah Jenis Dokumen', 'jenis_dokumen', 'far fa-plus-square', 57, 'Y'),
(29, 'Departement', 'departement', 'fas fa-plus-square', 57, 'Y'),
(30, 'Level Dokumen', 'leveld', 'fas fa-plus-square', 57, 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id_user` int(11) UNSIGNED NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `id_level` int(11) DEFAULT NULL,
  `id_bidang` int(11) DEFAULT NULL,
  `id_departement` int(11) NOT NULL,
  `is_active` enum('Y','N') DEFAULT 'Y',
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `username`, `full_name`, `password`, `id_level`, `id_bidang`, `id_departement`, `is_active`, `image`) VALUES
(1, 'admin', 'Administratorr', '$2y$05$JgSL8zuc5DIC/0MFLJ9qeewRNp7vyle1DIr6en8MN.xapN4fTQHQ6', 1, 1, 1, 'Y', ''),
(7, 'alvinkhan', 'alvin khanh rishad', '$2y$05$rwu8.3ag5UUlydIAfhghYe8GqWEKH7ItjEziV5tTWo2tYGNntVgPa', 6, 1, 1, 'Y', ''),
(12, 'RuslanHalim', 'Ruslan Halim', '$2y$05$/DmwrazjP4PVePykmAMba.z.O4TP8AiwQqqQ6iqRJ2usWqkI0xYB.', 4, 1, 1, 'Y', ''),
(13, 'Yahya', 'Yahya', '$2y$05$r4Y1GGo2Omrdnru3CPKbneeL7vJ8DpzDSNfke1aI8FWlMOiXFYlrG', 4, 1, 1, 'Y', ''),
(14, 'Muntako', 'Muntako', '$2y$05$GhWHZmYA91X75V6xMrOmDeQ4uNOUndMoX1tL/N36j2NhsTHibjd.u', 4, 2, 1, 'Y', ''),
(15, 'DoniIndriyanto', 'Doni Indriyanto', '$2y$05$CFDo4uZFnZdp30U9FehZcepmLwg3LiwUEMkk9ej30WbIpdt7Lpl6S', 2, 1, 1, 'Y', ''),
(16, 'IndraZubir', 'Indra Zubir', '$2y$05$su3bi0k4edFI9YQ28lTNj.0m71fF5ShauRq2FdfglTwuEVwrJG7lu', 2, 2, 1, 'Y', ''),
(17, 'DjokoSuharyanto', 'Djoko Suharyanto', '$2y$05$tROBJ/622VOJNmlaIbDcbuzkqCU4/oq8QYWH714nIRrDeFEPURnTq', 5, 1, 1, 'Y', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_userlevel`
--

CREATE TABLE `tbl_userlevel` (
  `id_level` int(11) UNSIGNED NOT NULL,
  `nama_level` varchar(50) DEFAULT NULL,
  `id_hirarki` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_userlevel`
--

INSERT INTO `tbl_userlevel` (`id_level`, `nama_level`, `id_hirarki`) VALUES
(1, 'Super Admin', 5),
(2, 'Kepala Departement', 3),
(3, 'user', 1),
(4, 'Admin QHSE', 2),
(5, 'Manager Departement', 4),
(6, 'Admin Departement', 2),
(7, 'Supervisor Departement', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aplikasi`
--
ALTER TABLE `aplikasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akses_menu`
--
ALTER TABLE `tbl_akses_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_akses_submenu`
--
ALTER TABLE `tbl_akses_submenu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
  ADD PRIMARY KEY (`id_bidang`),
  ADD KEY `id_departement` (`id_departement`);

--
-- Indexes for table `tbl_departement`
--
ALTER TABLE `tbl_departement`
  ADD PRIMARY KEY (`id_departement`);

--
-- Indexes for table `tbl_detail_dokumen`
--
ALTER TABLE `tbl_detail_dokumen`
  ADD PRIMARY KEY (`id_detail_dokumen`);

--
-- Indexes for table `tbl_dokumen`
--
ALTER TABLE `tbl_dokumen`
  ADD PRIMARY KEY (`id_dokumen`);

--
-- Indexes for table `tbl_hirarki`
--
ALTER TABLE `tbl_hirarki`
  ADD PRIMARY KEY (`id_hirarki`);

--
-- Indexes for table `tbl_jenis_dokumen`
--
ALTER TABLE `tbl_jenis_dokumen`
  ADD PRIMARY KEY (`id_jenis`);

--
-- Indexes for table `tbl_level_dokumen`
--
ALTER TABLE `tbl_level_dokumen`
  ADD PRIMARY KEY (`id_leveld`);

--
-- Indexes for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `tbl_progres`
--
ALTER TABLE `tbl_progres`
  ADD PRIMARY KEY (`id_progres`);

--
-- Indexes for table `tbl_progres_pengajuan`
--
ALTER TABLE `tbl_progres_pengajuan`
  ADD PRIMARY KEY (`id_progres_pengajuan`),
  ADD UNIQUE KEY `id_dokumen` (`id_dokumen`);

--
-- Indexes for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  ADD PRIMARY KEY (`id_submenu`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_level` (`id_level`),
  ADD KEY `id_bidang` (`id_bidang`),
  ADD KEY `id_departement` (`id_departement`);

--
-- Indexes for table `tbl_userlevel`
--
ALTER TABLE `tbl_userlevel`
  ADD PRIMARY KEY (`id_level`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aplikasi`
--
ALTER TABLE `aplikasi`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_akses_menu`
--
ALTER TABLE `tbl_akses_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=143;

--
-- AUTO_INCREMENT for table `tbl_akses_submenu`
--
ALTER TABLE `tbl_akses_submenu`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=266;

--
-- AUTO_INCREMENT for table `tbl_bidang`
--
ALTER TABLE `tbl_bidang`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_departement`
--
ALTER TABLE `tbl_departement`
  MODIFY `id_departement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_detail_dokumen`
--
ALTER TABLE `tbl_detail_dokumen`
  MODIFY `id_detail_dokumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_dokumen`
--
ALTER TABLE `tbl_dokumen`
  MODIFY `id_dokumen` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_hirarki`
--
ALTER TABLE `tbl_hirarki`
  MODIFY `id_hirarki` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_jenis_dokumen`
--
ALTER TABLE `tbl_jenis_dokumen`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_level_dokumen`
--
ALTER TABLE `tbl_level_dokumen`
  MODIFY `id_leveld` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_menu`
--
ALTER TABLE `tbl_menu`
  MODIFY `id_menu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `tbl_progres`
--
ALTER TABLE `tbl_progres`
  MODIFY `id_progres` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_progres_pengajuan`
--
ALTER TABLE `tbl_progres_pengajuan`
  MODIFY `id_progres_pengajuan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_submenu`
--
ALTER TABLE `tbl_submenu`
  MODIFY `id_submenu` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_userlevel`
--
ALTER TABLE `tbl_userlevel`
  MODIFY `id_level` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
