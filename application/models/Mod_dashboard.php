<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_dashboard extends CI_Model
{
    public function hitungJumlahDokumen()
    {   
        $query = $this->db->get('tbl_dokumen');
        if($query->num_rows()>0)
        {
          return $query->num_rows();
        }
        else
        {
          return 0;
        }
    }
 

    public function hitungJumlahDetail()
    {   
        $query = $this->db->get('tbl_detail_dokumen');
        if($query->num_rows()>0)
        {
          return $query->num_rows();
        }
        else
        {
          return 0;
        }
    }



}