<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Mod_jenis_dokumen extends CI_Model
{
	var $table = 'tbl_jenis_dokumen';
	var $column_search = array('id_jenis', 'nama_jenis', 'kode_jenis', 'id_user');
	var $column_order = array('id_jenis', 'nama_jenis', 'kode_jenis', 'id_user');
	var $order = array('id_jenis' => 'ascd');
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{
		$this->db->from('tbl_jenis_dokumen a');
		$this->db->select('a.*,b.nama_bidang, c.full_name, d.nama_leveld');
		$this->db->join('tbl_bidang b', 'a.id_bidang=b.id_bidang');
		$this->db->join('tbl_user c', 'a.id_user=c.id_user');
		$this->db->join('tbl_level_dokumen d', 'a.id_leveld=d.id_leveld');


		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getAll()
	{
		$this->db->select('a.*,b.nama_bidang, c.full_name');
		$this->db->join('tbl_bidang b', 'a.id_bidang=b.id_bidang');
		$this->db->join('tbl_user c', 'a.id_bidang=c.id_user');
		$this->db->join('tbl_level_dokumen d', 'a.id_leveld=d.id_leveld');
		$this->db->order_by('a.id_jenis desc');
		return $this->db->get('tbl_jenis_dokumen a');
	}

	function get_datatables()
	{
		$this->_get_datatables_query();
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

	function count_filtered()
	{
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	function count_all()
	{
		$this->db->from('tbl_jenis_dokumen');
		return $this->db->count_all_results();
	}

	function insert_jenis($table, $data)
	{
		$insert = $this->db->insert($table, $data);
		return $insert;
	}

	function update_jenis($id, $data)
	{
		$this->db->where('id_jenis', $id);
		$this->db->update('tbl_jenis_dokumen', $data);
	}

	function get_jenis($id)
	{
		$this->db->where('id_jenis', $id);
		return $this->db->get('tbl_jenis_dokumen')->row();
	}

	function delete_jenis($id, $table)
	{
		$this->db->where('id_jenis', $id);
		$this->db->delete($table);
	}

	function bidang()
	{
		return $this->db->order_by('id_bidang ASC')
			->get('tbl_bidang')
			->result();
	}

	function leveld()
	{
		return $this->db->order_by('id_leveld ASC')
			->get('tbl_level_dokumen')
			->result();
	}

	function user()
	{
		$this->db->where('id_level', 5);
		return $this->db->order_by('id_user ASC')
			->get('tbl_user')
			->result();
	}
}
