<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mod_created extends CI_Model
{

    var $table = 'tbl_dokumen';
    var $column_order = array('kode_dokumen', 'id_departement', 'id_dokumen', 'nama_dokumen', 'berkas', 'id_jenis', 'is_active', 'created_at');
    var $column_search = array('kode_dokumen', 'id_departement', 'id_dokumen', 'nama_dokumen', 'berkas', 'id_jenis', 'is_active', 'created_at');
    var $order = array('id_dokumen' => 'desc'); // default order 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    private function _get_datatables_query()
    {
        $this->db->select('a.*,b.full_name,c.nama_departement, d.nama_jenis, e.nama_leveld');
        $this->db->join('tbl_user b', 'a.id_user=b.id_user');
        $this->db->join('tbl_departement c', 'a.id_departement=c.id_departement');
        $this->db->join('tbl_jenis_dokumen d', 'a.id_jenis=d.id_jenis');
        $this->db->join('tbl_level_dokumen e', 'a.id_leveld=e.id_leveld');
        $this->db->from('tbl_dokumen a');
        $i = 0;

        foreach ($this->column_search as $item) // loop column 
        {
            if ($_POST['search']['value']) // if datatable send POST for search
            {

                if ($i === 0) // first loop
                {
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }

                if (count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }

        if (isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if (isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if ($_POST['length'] != -1)
            $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {

        $this->db->from('tbl_dokumen');
        return $this->db->count_all_results();
    }

    function view_dokumen($id)
    {
        $this->db->where('id_dokumen', $id);
        return $this->db->get('tbl_dokumen');
    }

    function getAll()
    {
        $this->db->select('a.*,b.full_name,c.nama_departement, d.nama_jenis, e.nama_leveld');
        $this->db->join('tbl_user b', 'a.id_user=b.id_user');
        $this->db->join('tbl_departement c', 'a.id_departement=c.id_departement');
        $this->db->join('tbl_jenis_dokumen d', 'a.id_jenis=d.id_jenis');
        $this->db->join('tbl_level_dokumen e', 'a.id_leveld=e.id_leveld');
        $this->db->order_by('a.id_dokumen desc');
        return $this->db->get('tbl_dokumen a');
    }

    function cekDokumen($namadokumen)
    {
        $this->db->where("nama_dokumen", $namadokumen);
        return $this->db->get("tbl_dokumen");
    }

    // function cekData()
    // {
    //     $nama_dokumen = $this->session->userdata('id_user');
    //     return $this->db->query("SELECT * FROM tbl_dokumen WHERE id_dokumen='$nama_dokumen' ORDER BY id_dokumen");
    // }


    function insertDokumen($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function insertDetail($tabel, $data)
    {
        $insert = $this->db->insert($tabel, $data);
        return $insert;
    }

    function getDokumen($id)
    {
        $this->db->where("id_dokumen", $id);
        return $this->db->get("tbl_dokumen a")->row();
    }

    function updateDokumen($id, $data)
    {
        $this->db->where('id_dokumen', $id);
        $this->db->update('tbl_dokumen', $data);
    }

    function getBerkas($id)
    {
        $this->db->select('berkas');
        $this->db->from('tbl_dokumen');
        $this->db->where('id_dokumen', $id);
        return $this->db->get();
    }



    function deleteDokumen($id, $table)
    {
        $this->db->where('id_dokumen', $id);
        $this->db->delete($table);
    }

    function user()
    {
        return $this->db->order_by('id_user ASC')
            ->get('tbl_user')
            ->result();
    }

    function bidang($id_departement = NULL)
    {
        $this->db->where('id_departement', $id_departement);
        $this->db->order_by('id_bidang', 'ASC');
        return $this->db->from('tbl_bidang')
            ->get()
            ->result();
    }


    function userr($id_departement = null)
    {
        $this->db->where('id_departement', $id_departement);
        $this->db->where('id_level', 5);
        $this->db->order_by('id_user', 'ASC');
        return $this->db->from('tbl_user')
            ->get()
            ->result();
    }


    function departement()
    {
        $this->db->order_by('id_departement', 'ASC');
        return $this->db->from('tbl_departement')
            ->get()
            ->result();
    }



    function jenis_dokumen()
    {
        $this->db->order_by('id_jenis', 'ASC');
        return $this->db->from('tbl_jenis_dokumen')
            ->get()
            ->result();
    }

    function leveld()
    {
        $this->db->order_by('id_leveld', 'ASC');
        return $this->db->from('tbl_level_dokumen')
            ->get()
            ->result();
    }

    function qhse()
    {
        $this->db->where('id_level', 4);
        return $this->db->order_by('id_user ASC')
            ->get('tbl_user')
            ->result();
    }

    function qhsee($id_user)
    {
        $this->db->where('id_user', $id_user);
        $this->db->order_by('id_jenis', 'ASC');
        return $this->db->from('tbl_jenis_dokumen')
            ->get()
            ->result();
    }

    // function akses()
    // {
    //     $this->db->select('a.*,b.add_level');
    //     $this->db->join('tbl_user b', 'a.id_level=b.id_level');
    //     $this->db->order_by('id_level', 'ASC');        
    //     return $this->db->from('tbl_akses_submenu')
    //         ->get()
    //         ->result();
    // }
}
