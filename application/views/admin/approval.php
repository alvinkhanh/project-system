    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">                     
                      <table class="table table-striped" id="mydata">
                          <thead>
                              <tr>
                                  <th>Kode Dokumen</th>
                                  <th>Nama Dokumen</th>
                                  <th>Diajukan Oleh</th>
                                  <th>Berkas</th>
                                  <th style="text-align: right;">Actions</th>
                              </tr>
                          </thead>
                          <tbody id="show_data">
                              
                          </tbody>
                      </table>              
            <script type="text/javascript">
                $(document).ready(function(){
                    show_product(); //call function show all product
                    
                    $('#mydata').dataTable();
                      
                    //function show all product
                    function show_product(){
                        $.ajax({
                            type  : 'ajax',
                            url   : '<?php echo site_url('aprove/dokumen_data')?>',
                            async : true,
                            dataType : 'json',
                            success : function(data){
                                var html = '';
                                var i;
                                for(i=0; i<data.length; i++){
                                    html += '<tr>'+
                                            '<td>'+data[i].kode_dokumen+'</td>'+
                                            '<td>'+data[i].nama_dokumen+'</td>'+
                                            '<td>'+data[i].berkas+'</td>'+
                                            '<td>'+data[i].id_user+'</td>'+
                                            '<td style="text-align:right;">'+
                                                '<a href="javascript:void(0);" class="btn btn-info btn-sm item_confirm" data-id_detail_dokumen="'+data[i].id_detail_dokumen+'">Confirm</a>'+' '+
                                                '<a href="javascript:void(0);" class="btn btn-danger btn-sm item_reject" data-id_detail_dokumen="'+data[i].id_detail_dokumen+'">Reject</a>'+
                                            '</td>'+
                                            '</tr>';
                                }
                                $('#show_data').html(html);
                            }
            
                        });
                    }
                });
            
            </script>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>

