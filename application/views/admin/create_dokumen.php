    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header bg-light">
                <h3 class="card-title"><i class="fa fa-list text-blue"></i> Data Dokumen</h3>
                <div class="text-right">
            
                  <button type="button" class="btn btn-sm btn-outline-primary" onclick="add_dokumen()" title="Add Data"><i class="fas fa-plus"></i> Add</button>
                
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabeluser" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr class="bg-info">
                      <th>Kode Dokumen</th>
                      <th>Departement</th>
                      <th>Full Name</th>
                      <th>Nama Dokumen</th>
                      <th>Jenis Dokumen</th>
                      <th>Level Dokumen</th>
                      <th>Waktu Uploud</th>
                      <th>Is Active</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>



    <div class="modal fade" id="modal-default">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title ">View Dokumen</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body text-center" id="md_def">
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <script type="text/javascript">
      var save_method; //for save method string
      var table;


      $(document).ready(function() {

        table = $("#tabeluser").DataTable({
          "responsive": true,
          "autoWidth": false,
          "language": {
            "sEmptyTable": "Data Dokumen Belum Ada"
          },
          "processing": true, //Feature control the processing indicator.
          "serverSide": true, //Feature control DataTables' server-side processing mode.
          "order": [], //Initial no order.

          // Load data for the table's content from an Ajax source
          "ajax": {
            "url": "<?php echo site_url('Created/ajax_list') ?>",
            "type": "POST"
          },
          //Set column definition initialisation properties.
          "columnDefs": [{
            "targets": [-1], //last column
            "render": function(data, type, row) {
              if (row[7] == "N") {
                return "<a class=\"btn btn-xs btn-outline-info\" href=\"javascript:void(0)\" title=\"View\" onclick=\"vdok(" + row[8] + ")\"><i class=\"fas fa-eye\"></i></a> <a class=\"btn btn-xs btn-outline-primary\"  href=\"javascript:void(0)\" title=\"Edit\" onclick=\"edit_dokumen(" + row[8] + ")\"><i class=\"fas fa-edit\"></i></a><a class=\"btn btn-xs btn-outline-danger\" href=\"javascript:void(0)\" title=\"Delete\"  onclick=\"deldokumen(" + row[8] + ")\"><i class=\"fas fa-trash\"></i></a>"
              } else {
                return "<a class=\"btn btn-xs btn-outline-info\" href=\"javascript:void(0)\" title=\"View\" onclick=\"vdok(" + row[8] + ")\"><i class=\"fas fa-eye\"></i></a> <a class=\"btn btn-xs btn-outline-primary\" href=\"javascript:void(0)\" title=\"Edit\" onclick=\"edit_dokumen(" + row[8] + ")\"><i class=\"fas fa-edit\"></i></a><a class=\"btn btn-xs btn-outline-primary\" href=\"javascript:void(0)\" title=\"download\" onclick=\"downdokumen(" + row[8] + ")\"><i class=\"fas fa-download\"></i></a>";
              }


            },
            "orderable": false, //set not orderable

          }, ],

        });
        $("input").change(function() {
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
          $(this).removeClass('is-invalid');
        });
        $("textarea").change(function() {
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
          $(this).removeClass('is-invalid');
        });
        $("select").change(function() {
          $(this).parent().parent().removeClass('has-error');
          $(this).next().empty();
          $(this).removeClass('is-invalid');
        });
      });

      function reload_table() {
        table.ajax.reload(null, false); //reload datatable ajax 
      }

      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
      });

      //view
      function vdok(id) {
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('.modal-title').text('View User');
        $("#modal-default").modal('show');
        $.ajax({
          url: '<?php echo base_url('Created/viewDokumen'); ?>',
          type: 'post',
          data: 'table=tbl_dokumen&id=' + id,
          success: function(respon) {
            $("#md_def").html(respon);
          }
        })
      }

      //delete
      function deldokumen(id) {
        Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
   if (result.value) {
    $.ajax({
      url:"<?php echo site_url('Created/delete');?>",
      type:"POST",
      data:"id="+id,
      cache:false,
      dataType: 'json',
      success:function(respone){
        if (respone.status == true) {
          reload_table();
          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
            );
        }else{
          Toast.fire({
            icon: 'error',
            title: 'Delete Error!!.'
          });
        }
      }
    });
  }else if (result.dismiss === Swal.DismissReason.cancel) {
    Swal(
      'Cancelled',
      'Your imaginary file is safe :)',
      'error'
      )
  }


})
}

      //download
      function downdokumen(id) {
        Swal.fire({
    title: 'Download This Document?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, download it!'
  }).then((result) => {
   if (result.value) {
    $.ajax({
      url:"<?php echo site_url('Created/downloadd');?>",
      type:"POST",
      data:"id="+id,
      cache:false,
      dataType: 'json',
      success:function(respone){
        if (respone.status == true) {
          reload_table();
          Swal.fire(
            'Deleted!',
            'Your file has been download.',
            'success'
            );
        }else{
          Toast.fire({
            icon: 'error',
            title: 'Delete Error!!.'
          });
        }
      }
    });
  }else if (result.dismiss === Swal.DismissReason.cancel) {
    Swal(
      'Cancelled',
      'error'
      )
  }


})
}

      function add_dokumen() {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Tambah Dokumen'); // Set Title to Bootstrap modal title
      }

      function edit_dokumen(id) {

        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
          url: "<?php echo site_url('Created/editdokumen') ?>/" + id,
          type: "GET",
          dataType: "JSON",
          success: function(data) {

            $('[name="id_dokumen"]').val(data.id_dokumen);
            $('[name="kode_dokumen"]').val(data.kode_dokumen);
            $('[name="nama_dokumen"]').val(data.nama_dokumen);
            $('[name="id_departement"]').val(data.id_departement);
            $('[name="id_leveld"]').val(data.id_leveld);
            $('[name="is_active"]').val(data.is_active);

            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Dokumen'); // Set title to Bootstrap modal title

          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert('Error get data from ajax');
          }
        });
      }

      function save() {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled', true); //set button disable 
        var url;
        if (save_method == 'add') {
          url = "<?php echo site_url('Created/insert') ?>";
        } else {
          url = "<?php echo site_url('Created/update') ?>";
        }
        var formdata = new FormData($('#form')[0]);
        $.ajax({
          url: url,
          type: "POST",
          data: formdata,
          dataType: "JSON",
          cache: false,
          contentType: false,
          processData: false,
          success: function(data) {

            if (data.status) //if success close modal and reload ajax table
            {
              $('#modal_form').modal('hide');
              reload_table();
              Toast.fire({
                icon: 'success',
                title: 'Success!!.'
              });
            } else {
              for (var i = 0; i < data.inputerror.length; i++) {
                $('[name="' + data.inputerror[i] + '"]').addClass('is-invalid');
                $('[name="' + data.inputerror[i] + '"]').closest('.kosong').append('<span></span>');
                $('[name="' + data.inputerror[i] + '"]').next().text(data.error_string[i]).addClass('invalid-feedback');
              }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable 


          },
          error: function(jqXHR, textStatus, errorThrown) {
            alert(textStatus);
            // alert('Error adding / update data');
            Toast.fire({
              icon: 'error',
              title: 'Error!!.'
            });
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled', false); //set button enable 

          }
        });
      }

      function batal() {
        $('#form')[0].reset();
        reload_table();
        var image = document.getElementById('v_image');
        image.src = "";
      }
    </script>


    <!-- Bootstrap modal -->
    <div class="modal fade" id="modal_form" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <h3 class="modal-title">Dokumen Form</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>

          </div>
          <div class="modal-body form">
            <form action="#" id="form" class="form-horizontal" enctype="multipart/form-data">
              <!-- <?php echo form_open_multipart('', array('class' => 'form-horizontal', 'id' => 'form')) ?> -->
              <div class="form-row">
                 <div class="col-md-4">
                  <div class="form-group">
                      <label for="berkas">Berkas </label>
                      <input type="file" class="form-control-file" id="berkas" name="berkas">
                  </div>
                </div>
                <div class="col-md-8">
                <label for="Nama Dokumen">Kode Dokumen</label>
                  <input type="text" class="form-control" name="kode_dokumen" id="kode_dokumen" placeholder="Kode Dokumen">
                  <input type="text" class="form-control" name="id_user" id="input" value="<?php echo $this->session->userdata('id_user') ?>" hidden readonly>
                </div>
                <div class="col-md-4">
                  <label for="Nama Dokumen">Nama Dokumen</label>
                  <input type="text" class="form-control" name="nama_dokumen" id="nama_dokumen" placeholder="Nama Dokumen">
                </div>
                <div class="col-md-4">
                <label for="id_departement">Departement</label>
                  <select id="id_departement" name="id_departement" class="form-control select2">
                    <option selected>Pilih Departement</option>
                    <?php
                    foreach ($hasil as $value) {
                      echo "<option value='$value->id_departement'>$value->nama_departement</option>";
                    }
                    ?>
                  </select>
                </div>
                <div class="col-md-4">
                <label for="inputState">Section</label>
                  <select id="id_bidang" name="id_bidang" class="form-control">
                    <option value="">Pilih Section</option>
                  </select>  
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="id_user">Aproval QHSE</label>
                  <select class="form-control" name="id_approval" id="id_qhse">
                    <option value="">Pilih Admin</option>
                    <?php
                    foreach ($qhse as $qh) { ?>
                      <option value="<?= $qh->id_user; ?>"><?= $qh->full_name; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="col-md-4">
                <label for="inputState">Kepala Departement</label>
                  <select id="id_user" name="id_approval" class="form-control">
                    <option selected>Pilih Manager</option>
                  </select>                      
                </div>
                <div class="form-group col-md-4">
                  <label for="inputState">Manager</label>
                  <select id="id_user" name="id_approval" class="form-control">
                    <option selected>Pilih Manager</option>
                  </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-4">
                  <label for="id_jenis">Jenis Dokumen</label>
                  <select id="id_jenis" name="id_jenis" class="form-control">
                    <option selected>Pilih Jenis Dokumen</option>
                  </select>
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label for="id_user">level Dokumen</label>
                  <select class="form-control" name="id_leveld" id="id_leveld">
                    <option value="">Pilih Level</option>
                    <?php
                    foreach ($leveld as $ld) { ?>
                      <option value="<?= $ld->id_leveld; ?>"><?= $ld->nama_leveld; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group col-md-4">
                  <label for="id_jenis">Active</label>
                  <select class="form-control" name="is_active" id="is_active">
                    <option value="Y">Y</option>
                    <option value="N">N</option>
                  </select>
                </div>
              </div>

              <!-- <?php echo form_close(); ?> -->
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
            <button type="button" class="btn btn-danger" onclick="batal()" data-dismiss="modal">Cancel</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <!-- End Bootstrap modal -->

    <script>
      // departement
      $("#id_departement").change(function() {

        // variabel dari nilai combo box kendaraan
        var id_departement = $("#id_departement").val();

        // Menggunakan ajax untuk mengirim dan dan menerima data dari server
        $.ajax({
          url: "<?php echo base_url(); ?>/Created/get_bidang",
          method: "POST",
          data: {
            id_departement: id_departement
          },
          async: false,
          dataType: 'json',
          success: function(data) {
            var html = '';
            var i;

            for (i = 0; i < data.length; i++) {
              html += '<option value=' + data[i].id_bidang + '>' + data[i].nama_bidang + '</option>';
            }
            $('#id_bidang').html(html);

          }
        });
      });

      $("#id_departement").change(function() {

        // variabel dari nilai combo box kendaraan
        var id_departement = $("#id_departement").val();

        // Menggunakan ajax untuk mengirim dan dan menerima data dari server
        $.ajax({
          url: "<?php echo base_url(); ?>/Created/get_userr",
          method: "POST",
          data: {
            id_departement: id_departement
          },
          async: false,
          dataType: 'json',
          success: function(data) {
            var html = '';
            var i;

            for (i = 0; i < data.length; i++) {
              html += '<option value=' + data[i].id_user + '>' + data[i].full_name + '</option>';
            }
            $('#id_user').html(html);

          }
        });
      });

      $("#id_qhse").change(function() {

        // variabel dari nilai combo box kendaraan
        var id_user = $("#id_qhse").val();

        // Menggunakan ajax untuk mengirim dan dan menerima data dari server
        $.ajax({
          url: "<?php echo base_url(); ?>/Created/get_qhsee",
          method: "POST",
          data: {
            id_user: id_user
          },
          async: false,
          dataType: 'json',
          success: function(data) {
            var html = '';
            var i;

            for (i = 0; i < data.length; i++) {
              html += '<option value=' + data[i].id_jenis + '>' + data[i].nama_jenis + '</option>';
            }
            $('#id_jenis').html(html);

          }
        });
      });
    </script>