<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('fungsi');
        $this->load->library('user_agent');
        $this->load->helper('myfunction_helper');
        $this->load->model('Mod_dashboard');
        // backButtonHandle();
    }
    

    function index()
    {
        $data['total_dokumen'] = $this->Mod_dashboard->hitungJumlahDokumen();
        $data['total_detail'] = $this->Mod_dashboard->hitungJumlahDetail();
    	$logged_in = $this->session->userdata('logged_in');
        if ($logged_in != TRUE || empty($logged_in)) {
            redirect('login');
        }else{
        	$this->template->load('layoutbackend','dashboard/dashboard_data', $data);
        }
        
    }



}
/* End of file Controllername.php */
 