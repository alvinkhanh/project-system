<?php
defined('BASEPATH') or exit('No direct script access allowed');


class departement extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_departement'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'departement');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_departement->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_departement; //array 0
            $row[] = $pel->nama_departement; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_departement->count_all(),
            "recordsFiltered" => $this->Mod_departement->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode = date('ymsi');
        $save  = array(
            'nama_departement' => $this->input->post('nama_departement')
        );
        $this->Mod_departement->insert_departement("tbl_departement", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_departement' => $this->input->post('nama_departement')
        );
        $this->Mod_departement->update_departement($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_departement($id)
    {
        $data = $this->Mod_departement->get_departement($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_departement->delete_departement($id, 'tbl_departement');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_departement') == '') {
            $data['inputerror'][] = 'nama_departement';
            $data['error_string'][] = 'Nama departement Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
