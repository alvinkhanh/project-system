<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Create By : Aryo
 * Youtube : Aryo Coding
 */
class Bidang extends MY_Controller
{
	
	function __construct()
	{
		parent::__construct();
        $this->load->model(array('Mod_bidang'));
	}

	public function index()
	{
		$this->load->helper('url');
        $data['departement'] = $this->Mod_bidang->departement();
        $this->template->load('layoutbackend','bidang',$data);
	}

	 public function ajax_list()
    {
        ini_set('memory_limit','512M');
        set_time_limit(3600);
        $list = $this->Mod_bidang->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_bidang;//array 0
            $row[] = $pel->nama_bidang;//array 1
            $row[] = $pel->nama_departement;//array 3
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Mod_bidang->count_all(),
                        "recordsFiltered" => $this->Mod_bidang->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode= date('ymsi');
		$save  = array(
            'nama_bidang'=> $this->input->post('nama_bidang'),
            'id_departement'=> $this->input->post('id_departement')
        );
            $this->Mod_bidang->insert_bidang("tbl_bidang", $save);
            echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_bidang' => $this->input->post('nama_bidang'),
            'id_departement'=> $this->input->post('id_departement')
        );
        $this->Mod_bidang->update_bidang($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_bidang($id)
    {
            $data = $this->Mod_bidang->get_bidang($id);
            echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_bidang->delete_bidang($id, 'tbl_bidang');        
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('nama_bidang') == '')
        {
            $data['inputerror'][] = 'nama_bidang';
            $data['error_string'][] = 'Nama bidang Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($this->input->post('id_departement') == '')
        {
            $data['inputerror'][] = 'id_departement';
            $data['error_string'][] = 'Nama Departement Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}