<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Created extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_created');
    }

    public function index()
    {
        $this->load->helper('url');
        $data['dokumen'] = $this->Mod_created->getAll();
        $data['user'] = $this->Mod_created->user();
        $data['userr'] = $this->Mod_created->userr();
        $data['departement'] = $this->Mod_created->departement();
        $data['hasil'] = $this->Mod_created->departement();
        $data['jenis'] = $this->Mod_created->jenis_dokumen();
        $data['qhse'] = $this->Mod_created->qhse();
        $data['leveld'] = $this->Mod_created->leveld();
        // $data['akses'] = $this->Mod_created->akses();
        $this->template->load('layoutbackend', 'admin/create_dokumen', $data);
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_created->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $berkas) {
            $no++;
            $row = array();
            $row[] = $berkas->kode_dokumen;
            $row[] = $berkas->nama_departement;
            $row[] = $berkas->full_name;
            $row[] = $berkas->nama_dokumen;
            $row[] = $berkas->nama_jenis;
            $row[] = $berkas->nama_leveld;
            $row[] = $berkas->created_at;
            $row[] = $berkas->is_active;
            $row[] = $berkas->id_dokumen;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_created->count_all(),
            "recordsFiltered" => $this->Mod_created->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        // var_dump($this->input->post('username'));
        $namadokumen = $this->input->post('nama_dokumen');
        $cek = $this->Mod_created->cekDokumen($namadokumen);
        if ($cek->num_rows() > 0) {
            echo json_encode(array("error" => "Nama Dokumen Sudah Ada!!"));
        } else {
            $nama = slug($this->input->post('nama_dokumen'));
            $config['upload_path']   = './assets/doc/user/';
            $config['allowed_types'] = 'pdf|xlsx|docx'; //mencegah upload backdor
            $config['file_name']     = $nama;

            $this->upload->initialize($config);

            if ($this->upload->do_upload('berkas')) {
                $berkas = $this->upload->data();

                $save  = array(
                    'kode_dokumen' => $this->input->post('kode_dokumen'),
                    'id_departement'  => $this->input->post('id_departement'),
                    'id_user'  => $this->input->post('id_user'),
                    'nama_dokumen' => $this->input->post('nama_dokumen'),
                    'id_leveld'  => $this->input->post('id_leveld'),
                    'id_jenis'  => $this->input->post('id_jenis'),
                    'berkas' => $berkas['file_name'],
                    'is_active'  => $this->input->post('is_active')
                );

                $this->Mod_created->insertDokumen("tbl_dokumen", $save);
                // $cek_id = $this->Mod_created->cekData()->row_array();
                $approval = $this->input->post('id_approval');

                for($i=0;$i<=3;$i++){
                    $data = array(
                        'id_user'  => $this->input->post('id_user'),
                        'id_approval' => $this->input->post('id_approval')
                    );
                $this->Mod_created->insertDetail("tbl_detail_dokumen", $data);
                }
                echo json_encode(array("status" => TRUE));
                } else {
                $save  = array(
                    'kode_dokumen' => $this->input->post('kode_dokumen'),
                    'nama_dokumen' => $this->input->post('nama_dokumen'),
                    'id_departement'  => $this->input->post('id_departement'),
                    'is_active'  => $this->input->post('is_active')

                );
                $this->Mod_created->insertDokumen("tbl_dokumen", $save);
                echo json_encode(array("status" => TRUE));
            }
        }
    }

    public function viewDokumen()
    {
        $id = $this->input->post('id');
        $table = $this->input->post('table');
        $data['table'] = $table;
        $data['data_field'] = $this->db->field_data($table);
        $data['data_table'] = $this->Mod_created->view_dokumen($id)->result_array();
        $this->load->view('admin/view', $data);
    }

    public function editdokumen($id)
    {
        $data = $this->Mod_created->getDokumen($id);
        echo json_encode($data);
    }


    public function update()
    {
        if(!empty($_FILES['berkas']['name'])) {
            $id = $this->input->post('id_dokumen');            
            $nama = slug($this->input->post('nama_dokumen'));
            $config['upload_path']   = './assets/doc/user/';
            $config['allowed_types'] = 'pdf|xlsx|docx'; //mencegah upload backdor
            $config['file_name']     = $nama; 
            
                $this->upload->initialize($config);
                
                if ($this->upload->do_upload('berkas')){
                $berkas = $this->upload->data();
                //Jika Password tidak kosong
                if ($this->input->post('id_leveld')) {
                    $save  = array(
                        'kode_dokumen' => $this->input->post('kode_dokumen'),
                        'id_departement'  => $this->input->post('id_departement'),
                        'id_user'  => $this->input->post('id_user'),
                        'nama_dokumen' => $this->input->post('nama_dokumen'),
                        'id_leveld'  => $this->input->post('id_leveld'),
                        'id_jenis'  => $this->input->post('id_jenis'),
                        'berkas' => $berkas['file_name'],
                        'is_active'  => $this->input->post('is_active')
                    );
                }else{//Jika password kosong
                    $save  = array(
                        'kode_dokumen' => $this->input->post('kode_dokumen'),
                        'id_departement'  => $this->input->post('id_departement'),
                        'id_user'  => $this->input->post('id_user'),
                        'nama_dokumen' => $this->input->post('nama_dokumen'),
                        'id_jenis'  => $this->input->post('id_jenis'),
                        'berkas' => $berkas['file_name'],
                        'is_active'  => $this->input->post('is_active')
                    );
                }
                
                
                $g = $this->Mod_created->getBerkas($id)->row_array();
    
                if ($g != null) {
                    //hapus gambar yg ada diserver
                    unlink('./assets/doc/user/'.$g['berkas']);
                }
                
                $this->Mod_created->updateDokumen($id, $save);
                echo json_encode(array("status" => TRUE));
                }else{//Apabila tidak ada gambar yang di upload
    
                     //Jika Password tidak kosong
                if ($this->input->post('id_leveld')) {
                    $save  = array(
                        'kode_dokumen' => $this->input->post('kode_dokumen'),
                        'id_departement'  => $this->input->post('id_departement'),
                        'id_user'  => $this->input->post('id_user'),
                        'nama_dokumen' => $this->input->post('nama_dokumen'),
                        'id_jenis'  => $this->input->post('id_jenis'),
                        'id_leveld'  => $this->input->post('id_leveld'),
                        'is_active'  => $this->input->post('is_active')
                    );
                }else{//Jika password kosong
                    $save  = array(
                        'kode_dokumen' => $this->input->post('kode_dokumen'),
                        'id_departement'  => $this->input->post('id_departement'),
                        'id_user'  => $this->input->post('id_user'),
                        'nama_dokumen' => $this->input->post('nama_dokumen'),
                        'id_jenis'  => $this->input->post('id_jenis'),
                        'is_active'  => $this->input->post('is_active')
                    );
                }
                 
                    $this->Mod_created->updateDokumen($id, $save);
                    echo json_encode(array("status" => TRUE));
                }
            }else{
                $id_dokumen = $this->input->post('id_dokumen');
                if ($this->input->post('nama_dokumen')) {
                    $save  = array(
                    'is_active' => $this->input->post('is_active')
                    );
                }else{
                    $save  = array(
                    'is_active' => $this->input->post('is_active')
                    );
                }
                
                $this->Mod_created->updateDokumen($id_dokumen, $save);
                echo json_encode(array("status" => TRUE));
            }
    }

    public function delete()
    {

        $id = $this->input->post('id');
        $g = $this->Mod_created->getBerkas($id)->row_array();
        unlink('assets/doc/user/' . $g['berkas']);
        $this->Mod_created->deletedokumen($id, 'tbl_dokumen');
        $data['status'] = TRUE;
        echo json_encode($data);
    }

    public function downloadd()
    {
        $id = $this->input->post('id');
        $this->load->helper('download');
        $fileinfo = $this->Mod_created->getBerkas($id);
        $file = 'assets/doc/user'.$fileinfo['berkas'];
        force_download($file, NULL);
    }


    public function download()
    {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'No');
        $sheet->setCellValue('B1', 'Username');
        $sheet->setCellValue('C1', 'Full name');
        $sheet->setCellValue('D1', 'password');
        $sheet->setCellValue('E1', 'level');
        $sheet->setCellValue('F1', 'berkas');
        $sheet->setCellValue('G1', 'Active');

        $user = $this->Mod_created->getAll()->result();
        $no = 1;
        $x = 2;
        foreach ($user as $row) {
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $row->username);
            $sheet->setCellValue('C' . $x, $row->full_name);
            $sheet->setCellValue('D' . $x, $row->password);
            $sheet->setCellValue('E' . $x, $row->nama_level);
            $sheet->setCellValue('F' . $x, $row->berkas);
            $sheet->setCellValue('F' . $x, $row->is_active);
            $x++;
        }
        $writer = new Xlsx($spreadsheet);
        $filename = 'laporan-User';

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');
    }


    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('username') == '') {
            $data['inputerror'][] = 'username';
            $data['error_string'][] = 'Username is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('full_name') == '') {
            $data['inputerror'][] = 'full_name';
            $data['error_string'][] = 'Full Name is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('password') == '') {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'Password is required';
            $data['status'] = FALSE;
        }

        if ($this->input->post('is_active') == '') {
            $data['inputerror'][] = 'is_active';
            $data['error_string'][] = 'Please select Is Active';
            $data['status'] = FALSE;
        }

        if ($this->input->post('level') == '') {
            $data['inputerror'][] = 'level';
            $data['error_string'][] = 'Please select is level';
            $data['status'] = FALSE;
        }

        /*if($this->input->post('berkas') == '')
        {
            $data['inputerror'][] = 'berkas';
            $data['error_string'][] = 'berkas is required';
            $data['status'] = FALSE;
        }*/

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }

    function get_bidang()
    {
        $id_departement = $this->input->post('id_departement');
        $data = $this->Mod_created->bidang($id_departement);
        echo json_encode($data);
    }

    function get_userr()
    {
        $id_departement = $this->input->post('id_departement');
        $data = $this->Mod_created->userr($id_departement);
        echo json_encode($data);
    }

    function get_qhsee()
    {
        $id_user = $this->input->post('id_user');
        $data = $this->Mod_created->qhsee($id_user);
        echo json_encode($data);
    }
    
}
