<?php
defined('BASEPATH') or exit('No direct script access allowed');


class leveld extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_leveldokumen'));
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'level_dokumen');
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_leveldokumen->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_leveld; //array 0
            $row[] = $pel->nama_leveld; //array 1
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_leveldokumen->count_all(),
            "recordsFiltered" => $this->Mod_leveldokumen->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode = date('ymsi');
        $save  = array(
            'nama_leveld' => $this->input->post('nama_leveld')
        );
        $this->Mod_leveldokumen->insert_leveld("tbl_level_dokumen", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_leveld' => $this->input->post('nama_leveld')
        );
        $this->Mod_leveldokumen->update_leveld($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_leveld($id)
    {
        $data = $this->Mod_leveldokumen->get_leveld($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_leveldokumen->delete_leveld($id, 'tbl_level_dokumen');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_leveld') == '') {
            $data['inputerror'][] = 'nama_leveld';
            $data['error_string'][] = 'Nama leveld Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
