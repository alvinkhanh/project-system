<?php
defined('BASEPATH') or exit('No direct script access allowed');
class aprove extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Mod_aprove');
    }

    public function index()
    {
        $this->load->helper('url');
        $this->template->load('layoutbackend', 'admin/approval');
    }
    
    function dokumen_data(){
        $data=$this->Mod_aprove->detail_dokumen();
        echo json_encode($data);
    }
 
    function confirm(){
        $data=$this->Mod_aprove->save_product();
        echo json_encode($data);
    }
 
    function reject(){
        $data=$this->Mod_aprove->delete_product();
        echo json_encode($data);
    }
}
