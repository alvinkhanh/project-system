<?php
defined('BASEPATH') or exit('No direct script access allowed');


class jenis_dokumen extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Mod_jenis_dokumen'));
    }

    public function index()
    {
        $this->load->helper('url');
        $data['jenis_dokumen'] = $this->Mod_jenis_dokumen->getAll();
        $data['bidang'] = $this->Mod_jenis_dokumen->bidang();
        $data['leveld'] = $this->Mod_jenis_dokumen->leveld();
        $data['user'] = $this->Mod_jenis_dokumen->user();
        $this->template->load('layoutbackend', 'tambah_jenis', $data);
    }

    public function ajax_list()
    {
        ini_set('memory_limit', '512M');
        set_time_limit(3600);
        $list = $this->Mod_jenis_dokumen->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $pel) {
            $no++;
            $row = array();
            $row[] = $pel->id_jenis; //array 0
            $row[] = $pel->nama_leveld; //array 1
            $row[] = $pel->nama_jenis; //array 1
            $row[] = $pel->nama_bidang;
            $row[] = $pel->full_name;
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->Mod_jenis_dokumen->count_all(),
            "recordsFiltered" => $this->Mod_jenis_dokumen->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function insert()
    {
        $this->_validate();
        $kode = date('ymsi');
        $save  = array(
            'nama_jenis' => $this->input->post('nama_jenis'),
            'id_bidang' => $this->input->post('id_bidang'),
            'id_leveld' => $this->input->post('id_leveld'),
            'id_user' => $this->input->post('id_user')
        );
        $this->Mod_jenis_dokumen->insert_jenis("tbl_jenis_dokumen", $save);
        echo json_encode(array("status" => TRUE));
    }

    public function update()
    {
        $this->_validate();
        $id      = $this->input->post('id');
        $save  = array(
            'nama_jenis' => $this->input->post('nama_jenis'),
            'id_bidang' => $this->input->post('id_bidang'),
            'id_leveld' => $this->input->post('id_leveld'),
            'id_user' => $this->input->post('id_user')

        );
        $this->Mod_jenis_dokumen->update_jenis($id, $save);
        echo json_encode(array("status" => TRUE));
    }

    public function edit_jenis($id)
    {
        $data = $this->Mod_jenis_dokumen->get_jenis($id);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = $this->input->post('id');
        $this->Mod_jenis_dokumen->delete_jenis($id, 'tbl_jenis_dokumen');
        echo json_encode(array("status" => TRUE));
    }
    private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if ($this->input->post('nama_jenis') == '') {
            $data['inputerror'][] = 'nama_jenis';
            $data['error_string'][] = 'Nama Jenis Tidak Boleh Kosong';
            $data['status'] = FALSE;
        }

        if ($data['status'] === FALSE) {
            echo json_encode($data);
            exit();
        }
    }
}
